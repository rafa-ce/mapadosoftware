const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const servicoOferecidoSchema = new Schema(
  {
    servicoOferecidoTipo: { type: String, required: true },
    nomeDoSoftware: { type: String }
  }
)

const expertiseSchema = new Schema(
  {
    areaDeAtuacao: { type: String, required: true },
    servicoOferecido: [servicoOferecidoSchema]
  }
);

const midiaSocialSchema = new Schema(
  { 
    plataforma: { type: String  },
    nomeDoUsuario: {type: String }
  }
);

const Point = new Schema(
  {
    cnpj: { type: String },
    emailParaContato: { type: String, required: true },
    expertise: [expertiseSchema],
    midiaSocial: [midiaSocialSchema],
    nomeDaEmpresa: { type: String, require: true },
    nomeParaContato: { type: String, required: true },
    position: {
      lat: { type: Number, required: true },
      lng: { type: Number, required: true }
    },
    raioDeAtuacao: { type: String, required: true },
    resumoDaEmpresa: { type: String, require: true },
    siteURL: { type: String, required: true },
    telefoneParaContato: { type: String }
  },
  { timestamps: true }
);

module.exports = mongoose.model('point', Point);