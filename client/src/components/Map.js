import React from "react";
import mapboxgl from "mapbox-gl";
import turf from "turf";
import api from "../api";
import PropTypes from "prop-types";
import MapboxGeocoder from "@mapbox/mapbox-gl-geocoder";
import MapboxLanguage from "@mapbox/mapbox-gl-language";

mapboxgl.accessToken =
  "pk.eyJ1IjoiaW5pY2lhdGl2YSIsImEiOiJja2EzNmdmaGwwbDVkM2d0YXhpMmIwdXM2In0.md1yyMUsj-NRLQs29Ze-6w";

const generateInnerHTML = (properties) => {
  let HTMLString = "<div class='popupHTML'>";
  console.log(properties);
  HTMLString += properties.nomeDaEmpresa
    ? `<h3>${properties.nomeDaEmpresa}</h3>`
    : "";
  HTMLString += properties.resumoDaEmpresa
    ? `<p class='popup-sectionEntry'>${properties.resumoDaEmpresa}</p>`
    : "";
  HTMLString += properties.siteURL
    ? `<p class='popup-sectionHeader'><a href='${properties.siteURL}'>${properties.siteURL}</a></p>`
    : "";
  const parsedExpertise = JSON.parse(properties.expertise);
  if (parsedExpertise.length > 0) {
    HTMLString += "<div class='popup-section'>";
    HTMLString += "<p class='popup-sectionHeader'>Serviços Oferecidos</p>";
    parsedExpertise.forEach((expertise) => {
      HTMLString += `<p class='popup-sectionEntry'>${expertise.nomeDoSoftware} - ${expertise.servicoOferecido}`;
    });
    HTMLString += "</div>";
  }
  HTMLString += properties.raioDeAtuacao
    ? `<p class='popup-sectionHeader'>Raio de Atuação</p><p class='popup-sectionEntry'>${properties.raioDeAtuacao}</p>`
    : "";
  const parsedMidiaSocial = JSON.parse(properties.midiaSocial);
  if (parsedMidiaSocial.length > 0) {
    HTMLString += "<div class='popup-section'>";
    HTMLString += "<p class='popup-sectionHeader'>Mídia Social</p>";
    parsedMidiaSocial.forEach((midiaSocial) => {
      HTMLString += `<p class='popup-sectionEntry'>${midiaSocial.plataforma} - ${midiaSocial.nomeDoUsuario}</p>`;
    });
  }
  HTMLString += properties.cnpj
    ? `<p class="popup-sectionHeader">CNPJ</p><p class="popup-sectionEntry">${properties.cnpj}</p>`
    : "";
  HTMLString += "</div>";
  return HTMLString;
};

const getPointCollection = async (map) => {
  let pointCollection;
  await api
    .getAllPoints()
    .then((points) => {
      return points.data.data;
    })
    .then((data) => {
      const pointArray = data.map((point) => {
        const {
          cnpj,
          emailParaContato,
          expertise,
          midiaSocial,
          nomeDaEmpresa,
          NomeParaContato,
          raioDeAtuacao,
          resumoDaEmpresa,
          siteURL,
          telefoneParaContato,
          position,
        } = point;
        return turf.point([position.lng, position.lat], {
          cnpj,
          emailParaContato,
          expertise,
          midiaSocial,
          nomeDaEmpresa,
          NomeParaContato,
          raioDeAtuacao,
          resumoDaEmpresa,
          siteURL,
          telefoneParaContato,
        });
      });
      pointCollection = turf.featureCollection(pointArray);
      return pointCollection;
    })
    .then((collection) => {
      map.getSource("pointCollection").setData(collection);
    })
    .catch((error) => {
      console.error(error);
    });
};

export default class Map extends React.Component {
  mapRef = React.createRef();

  componentDidMount = async () => {
    this.map = new mapboxgl.Map({
      container: this.mapRef.current,
      style: "mapbox://styles/iniciativa/cka372ipk04sp1is6qxybu5i6",
      center: [-55.16, -16.28],
      zoom: 3.7,
    });
    const marker = new mapboxgl.Marker({ draggable: true, color: "#3658D6" });

    this.map.on("load", () => {
      this.map.addSource("pointCollection", {
        type: "geojson",
        data: turf.featureCollection([]),
        cluster: true,
        clusterMaxZoom: 14,
        clusterRadius: 50,
      });

      this.map.addLayer({
        id: "clusters",
        type: "circle",
        source: "pointCollection",
        filter: ["has", "point_count"],
        paint: {
          // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
          // with three steps to implement three types of circles:
          // * Blue, 20px circles when point count is less than 100
          // * Yellow, 30px circles when point count is between 100 and 750
          // * Pink, 40px circles when point count is greater than or equal to 750
          "circle-color": [
            "step",
            ["get", "point_count"],
            "#223b53",
            100,
            "#fbb03b",
            750,
            "#e55e5e",
          ],
          "circle-radius": [
            "step",
            ["get", "point_count"],
            20,
            100,
            30,
            750,
            40,
          ],
        },
      });

      this.map.addLayer({
        id: "cluster-count",
        type: "symbol",
        source: "pointCollection",
        filter: ["has", "point_count"],
        paint: {
          "text-color": "white",
        },
        layout: {
          "text-field": "{point_count_abbreviated}",
          "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
          "text-size": 12,
        },
      });

      this.map.addLayer({
        id: "unclustered-point",
        source: "pointCollection",
        type: "circle",
        filter: ["!", ["has", "point_count"]],
        paint: {
          "circle-color": "#000000",
          "circle-radius": 4,
          "circle-stroke-width": 1,
          "circle-stroke-color": "#fff",
        },
      });
      getPointCollection(this.map);
    });

    this.map.on("click", "clusters", (e) => {
      const features = this.map.queryRenderedFeatures(e.point, {
        layers: ["clusters"],
      });
      const clusterId = features[0].properties.cluster_id;
      this.map
        .getSource("pointCollection")
        .getClusterExpansionZoom(clusterId, (err, zoom) => {
          if (err) return;

          this.map.easeTo({
            center: features[0].geometry.coordinates,
            zoom: zoom,
          });
        });
    });
    this.map.on("click", "unclustered-point", (e) => {
      const coordinates = e.features[0].geometry.coordinates.slice();
      const properties = e.features[0].properties;
      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }
      const innerHTML = generateInnerHTML(properties);
      new mapboxgl.Popup()
        .setLngLat(coordinates)
        .setHTML(innerHTML)
        .addTo(this.map);
    });

    this.map.on("mouseenter", "unclustered-point", () => {
      this.map.getCanvas().style.cursor = "pointer";
    });
    this.map.on("mouseleave", "unclustered-point", () => {
      this.map.getCanvas().style.cursor = "";
    });

    this.map.on("click", (e) => {
      if (
        this.map.queryRenderedFeatures(e.point, {
          layers: ["clusters", "unclustered-point"],
        }).length
      )
        return;

      const { lng, lat } = e.lngLat;
      this.setState({ lng: lng.toFixed(4), lat: lat.toFixed(4) });
      const position = turf.point([lng.toFixed(4), lat.toFixed(4)]);

      if (marker.getLngLat()) {
        marker.setLngLat([lng, lat]);
      } else {
        marker.setLngLat([lng, lat]).addTo(this.map);
      }

      this.props.getPosition({ lng: lng.toFixed(4), lat: lat.toFixed(4) });
    });

    const geocoder = new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      language: "pt-BR",
      mapboxgl: mapboxgl,
      color: "#3658D6",
      marker: false,
    });

    document.getElementById("geocoder").appendChild(geocoder.onAdd(this.map));

    geocoder.on("result", (ev) => {
      const { geometry, place_name } = ev.result;

      const [lng, lat] = geometry.coordinates;

      if (marker.getLngLat()) {
        marker.setLngLat([lng, lat]);
      } else {
        marker.setLngLat([lng, lat]).addTo(this.map);
      }

      if (lng && lat) {
        this.props.getPosition({
          lng: lng.toFixed(4),
          lat: lat.toFixed(4),
        });
      }
    });

    this.map.addControl(new MapboxLanguage());
    this.map.addControl(new mapboxgl.NavigationControl());
  };

  render() {
    return <div ref={this.mapRef} id="map"></div>;
  }
}
Map.propTypes = {
  getPosition: PropTypes.func,
};
