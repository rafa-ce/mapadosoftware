import React, { useEffect, useState } from "react";
import api from "../api";
import PropTypes from "prop-types";
import { Formik, Form, Field, FieldArray, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Expertise } from './Expertise';
import './Form.css';

const formSchema = Yup.object().shape({
  nomeDaEmpresa: Yup.string()
    .min(2, "O nome da empresa Exige mais de 2 caractéres.")
    .max(50, "Somente 50 caractéres permitidos.")
    .required("Nome da empresa é Obrigatório"),
  cnpj: Yup.string()
    .matches(
      /\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}/,
      " CNPJ tem o formato 00.000.000/0001-00"
    )
    .notRequired(),
  resumoDaEmpresa: Yup.string()
    .max(140, "Somente 140 caractéres permitidos.")
    .required("Resumo da empresa e obrigatório"),
  siteURL: Yup.string().required("Site da empresa é obrigatório"),
  nomeParaContato: Yup.string()
    .min(2, "O nome para contato exige mais de 2 caractéres.")
    .max(140, "Somente 140 caractéres permitidos.")
    .required("Obrigatório"),
  emailParaContato: Yup.string()
    .matches(
      /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
      "email mal definido contato@xyz.com.br"
    )
    .required("Email é obrigatorio"),
  telefoneParaContato: Yup.string()
    .max(25, "Somente 25 caractéres permitidos.")
    .notRequired(),
  raioDeAtuacao: Yup.string().required("Raio de atuação é obrigatório"),
  midiaSocial: Yup.array()
    .of(
      Yup.object().shape({
        plataforma: Yup.string()
          .min(2, "O nome da plataforma exige mais de 2 caractéres.")
          .max(50, "Somente 50 caractéres permitidos."),
        nomeDoUsuario: Yup.string().when("plataforma", {
          is: (plataforma) => plataforma != null,
          then: Yup.string()
            .min(2, "O nome do usuário exige mais de 2 caractéres.")
            .max(50, "Somente 50 caractéres permitidos.")
            .required("URL, telefone ou @usuário é obrigatório"),
          otherwise: Yup.string(),
        }),
      })
    )
    .notRequired(),
  expertise: Yup.array()
    .of(
      Yup.object().shape({
        areaDeAtuacao: Yup.string()
          .required("Área de atuação é obrigatório"),
        servicoOferecido: Yup.array()
          .required("É obrigatório selecionar pelo menos um tipo de serviço."),
      })
    )
    .required("Expertise é um campo  obrigatório"),
});

export const PointForm = (props) => {
  const [showSuccess, setSuccess] = useState(false);
  const [error, setError] = useState(null);

  // auto hide success after a while
  useEffect(() => {
    if (showSuccess) {
      const tid = setTimeout(() => {
        alert(
          "Formulário enviado com sucesso, obrigado pela sua participação."
        );
        setSuccess(false);
      }, 2000);

      return () => clearTimeout(tid);
    }
  }, [showSuccess]);

  // auto hide success after a while
  useEffect(() => {
    if (error) {
      const tid = setTimeout(() => {
        setError(null);
      }, 5000);

      return () => clearTimeout(tid);
    }
  }, [error]);

  const handleSubmit = async (values, { resetForm, setSubmitting }) => {
    const { position } = props;
    const payload = {
      ...values,
      position,
    };

    try {
      await api.insertPoint(payload);
      setSuccess(true);
      resetForm();
    } catch (err) {
      setError(err);
      console.error(err);
    } finally {
      setSubmitting(false);
    }
  };

  const submitButtonClasses = (isValid) => {
    return `pure-button pure-button-primary ${
      isValid ? "" : "pure-button-disabled"
    }`;
  };

  const fieldClasses = (error, touched) => {
    return `pure-input-1 ${error && touched ? "error" : ""}`;
  };

  return (
    <div>
      {showSuccess && <span>O local foi enviado com sucesso.</span>}
      {error && <span>{error.message}</span>}
      <Formik
        initialValues={{
          nomeDaEmpresa: "",
          cnpj: "",
          resumoDaEmpresa: "",
          siteURL: "",
          nomeParaContato: "",
          emailParaContato: "",
          telefoneParaContato: "",
          raioDeAtuacao: "",
          midiaSocial: [{ plataforma: "", nomeDoUsuario: "" }], // dynamic form element
          expertise: [{ areaDeAtuacao: "", servicoOferecido: [] }], // dynamic form element
        }}
        validationSchema={formSchema}
        onSubmit={handleSubmit}
        >
        {({ values, errors, touched, isSubmitting, isValid }) => (
          <Form id="formWrapper" className="pure-form pure-form-stacked">
            <h2>Informações da Empresa</h2>
            <div className="pure-control-group">
              <label htmlFor="nomeDaEmpresa">
                <strong>Nome da Empresa/Prestadora/Cooperativa</strong>
              </label>
              <Field
                name="nomeDaEmpresa"
                placeholder="Minha Empresa S.A."
                className={fieldClasses(
                  errors.nomeDaEmpresa,
                  touched.nomeDaEmpresa
                )}
              />
              {errors.nomeDaEmpresa && touched.nomeDaEmpresa ? (
                <div className="pure-form-message error">
                  {errors.nomeDaEmpresa}
                </div>
              ) : null}
            </div>
            <div className="pure-control-group">
              <label htmlFor="cnpj">CNPJ</label>
              <Field
                name="cnpj"
                placeholder="00.000.000/0001-00"
                className={fieldClasses(errors.cnpj, touched.cnpj)}
              />
              {errors.cnpj && touched.cnpj ? (
                <div className="pure-form-message error">{errors.cnpj}</div>
              ) : null}
            </div>
            <div className="pure-control-group">
              <label htmlFor="resumoDaEmpresa"><strong>Resumo Da Empresa</strong></label>
              <Field
                id="resumoDaEmpresa"
                name="resumoDaEmpresa"
                as="textarea"
                placeholder="A nossa empresa se especializa em..."
                className={fieldClasses(
                  errors.resumoDaEmpresa,
                  touched.resumoDaEmpresa
                )}
              />
              {errors.resumoDaEmpresa && touched.resumoDaEmpresa ? (
                <div className="pure-form-message error">
                  {errors.resumoDaEmpresa}
                </div>
              ) : null}
            </div>
            <div className="pure-control-group">
              <label htmlFor="siteURL"><strong>Site</strong></label>
              <Field
                id="siteURL"
                name="siteURL"
                placeholder="https://mysite.com"
                className={fieldClasses(errors.siteURL, touched.siteURL)}
              />
              {errors.siteURL && touched.siteURL ? (
                <div className="pure-form-message error">{errors.siteURL}</div>
              ) : null}
            </div>

            <div className="pure-control-group">
              <label htmlFor="raioDeAtuacao"><strong>Raio de Atuação</strong></label>
              <Field
                id="raioDeAtuacao"
                name="raioDeAtuacao"
                as="select"
                className={fieldClasses(
                  errors.raioDeAtuacao,
                  touched.raioDeAtuacao
                )}
              >
                <option value="">Selecione raio</option>
                <option value="regional">Regional</option>
                <option value="estadual">Estadual</option>
                <option value="nacional">Nacional</option>
                <option value="internacional">Internacional</option>
              </Field>
              {errors.raioDeAtuacao && touched.raioDeAtuacao ? (
                <div className="pure-form-message error">
                  {errors.raioDeAtuacao}
                </div>
              ) : null}
            </div>
            
            <div className="pure-control-group">
              <label htmlFor='expertise'>
                <strong>Escolha uma área de atuação</strong>
              </label>
              <Expertise
                values={ values.expertise }
                errors={ errors.expertise }
                touched={ touched.expertise }
                checkClassName = { fieldClasses }
              ></Expertise>
            </div>

            <label htmlFor="midiaSocial">Mídia Social</label>
            <FieldArray
              name="midiaSocial"
              render={(arrayHelpers) => (
                <div>
                  {values.midiaSocial.map((plataforma, index) => {
                    const errorPlataforma =
                      errors?.midiaSocial?.[index]?.plataforma;
                    const errorNomeDoUsuario =
                      errors?.midiaSocial?.[index]?.nomeDoUsuario;
                    const touchedPlataforma =
                      touched?.midiaSocial?.[index]?.plataforma;
                    const touchedNomeDoUsuario =
                      touched?.midiaSocial?.[index]?.nomeDoUsuario;
                    return (
                      <div key={index} className="pure-control-group">
                        <div className="midiaSocial-wrapper">
                          <div className="midiaSocial-input-wrapper">
                            <Field
                              as="select"
                              name={`midiaSocial.${index}.plataforma`}
                              className={fieldClasses(
                                errorPlataforma,
                                touchedPlataforma
                              )}
                              >
                                <option value="">Plataforma</option>
                                <option value="Facebook">Facebook</option>
                                <option value="Instagram">Instagram</option>
                                <option value="Linkedln">Linkedln</option>
                                <option value="Mastodon">Mastodon</option>
                                <option value="Telegram">Telegram</option>
                                <option value="Twitter">Twitter</option>
                                <option value="Youtube">Youtube</option>
                                <option value="Whatsapp">Whatsapp</option>
                                <option value="Outro">Outro</option>
                            </Field>
                            {values.midiaSocial.length > 1 && (
                              <button
                                type="button"
                                title="remover plataforma social"
                                className="button-remove pure-button"
                                onClick={() => arrayHelpers.remove(index)}
                              >
                                {" "}
                                -{" "}
                              </button>
                            )}
                          </div>
                          {errorPlataforma && touchedPlataforma ? (
                            <div className="pure-form-message error">
                              {errorPlataforma}
                            </div>
                          ) : null}
                        </div>
                        <Field
                          name={`midiaSocial.${index}.nomeDoUsuario`}
                          placeholder="URL, telefone ou @usuário"
                          className={fieldClasses(
                            errorNomeDoUsuario,
                            touchedNomeDoUsuario
                          )}
                        />
                        {errorNomeDoUsuario && touchedNomeDoUsuario ? (
                          <div className="pure-form-message error">
                            {errorNomeDoUsuario}
                          </div>
                        ) : null}
                      </div>
                    );
                  })}
                  <div className="pure-control-group">
                    <button
                      type="button"
                      title="adicionar outra plataforma social"
                      className="button-add pure-button"
                      onClick={() =>
                        arrayHelpers.push({
                          plataforma: "",
                          nomeDoUsuario: "",
                        })
                      }
                    >
                      +
                    </button>
                    <span className="pure-form-message-inline">
                      Adicionar outra plataforma
                    </span>
                  </div>
                </div>
              )}
            />
            <h2>Informações Pessoais</h2>
            <div className="pure-control-group">
              <label htmlFor="nomeParaContato"><strong>Nome para Contato</strong></label>
              <Field
                id="nomeParaContato"
                name="nomeParaContato"
                placeholder="Juliana Da Silva"
                className={fieldClasses(
                  errors.nomeParaContato,
                  touched.nomeParaContato
                )}
              />
              {errors.nomeParaContato && touched.nomeParaContato ? (
                <div className="pure-form-message error">
                  {errors.nomeParaContato}
                </div>
              ) : null}
            </div>
            <div className="pure-control-group">
              <label htmlFor="emailParaContato"><strong>Email para Contato</strong></label>
              <Field
                id="emailParaContato"
                name="emailParaContato"
                placeholder="seuemail@exemplo.com"
                className={fieldClasses(
                  errors.emailParaContato,
                  touched.emailParaContato
                )}
              />
              {errors.emailParaContato && touched.emailParaContato ? (
                <div className="pure-form-message error ">
                  {errors.emailParaContato}
                </div>
              ) : null}
            </div>
            <div className="pure-control-group">
              <label htmlFor="telefoneParaContato">Telefone para Contato</label>
              <Field
                id="telefoneParaContato"
                name="telefoneParaContato"
                placeholder="(12) 1212-1212"
                className={fieldClasses(
                  errors.telefoneParaContato && touched.telefoneParaContato
                )}
              />
              {errors.telefoneParaContato && touched.telefoneParaContato ? (
                <div className="pure-form-message error">
                  {errors.telefoneParaContato}
                </div>
              ) : null}
            </div>
            <button
              type="submit"
              disabled={isSubmitting}
              className={submitButtonClasses(isValid)}
            >
              Enviar
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

PointForm.propTypes = {
  position: PropTypes.exact({
    lng: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    lat: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
};