import React, { Fragment } from 'react';
import {PointForm} from './Form';
import PropTypes from 'prop-types';
import { Tabs, Tab } from 'react-materialize';
import logo from '../images/Logo_MSA.png'

import './Sidebar.css';

export default function Sidebar(props) {
    const {position} = props;
    
    return (
        <div id="sidebar">
            <div className="sidebar-main">
                <div className="sidebar-header">
                    
                    <img className="logomsa" src={logo} alt="logo_msa" />
                    
                    <div id="geocoder"></div>
                </div>
                <div className="sidebar-nav">
                    <Tabs className="z-depth-1 tabs-fixed-width">
                        <Tab
                            active
                            options={{
                                duration: 300,
                                onShow: null,
                                responsiveThreshold: Infinity,
                                swipeable: false
                            }}
                            title="Cadastro"
                        >
                            { pointForm(position) }

                        </Tab>
                        <Tab
                            options={{
                                duration: 300,
                                onShow: null,
                                responsiveThreshold: Infinity,
                                swipeable: false
                            }}
                            title="Empresas"
                        >
                            <h5>Empresas cadastradas</h5>
                        </Tab>
                    </Tabs>   
                </div>
            </div>
            

            
            <footer className="page-footer">
                <div className="footer-copyright">
                    <div className="container">
                        <p>
                            Os dados desse site estão disponibilizado com uma licença <a
                            href="https://creativecommons.org/licenses/by-sa/3.0/igo/.">
                                CC BY-SA 3.0 IGO
                            </a>
                        </p>
                    </div>
                </div>
            </footer>
            
        </div>
    )
}

function pointForm(position) {
    if (position.lat && position.lng) {
        return (
            <PointForm position={position}/>
        )
    } 
    else {
        return (
            <Fragment>
                <p>
                    Este mapa tem como objetivo coletar e disponibilizar um catálogo de prestadores que 
                    oferecem serviços baseados em software livre ou recursos abertos para a área da educação. 
                    Com isso, buscamos facilitar o contato entre prestadores e gestores, e promovendo o livre 
                    e o aberto na educação! O prestador não precisa exclusivamente trabalhar com software 
                    livre, hardware livre ou recursos educacionais abertos mas deve ter expertise e uma carta 
                    de serviços nessa área. Se você é prestador e deseja se cadastrar, insira seus dados abaixo. 
                    Se você é gestor, use o menu para encontrar prestadores de serviço.
                </p>

                <p><strong>Click no mapa ou localize seu escritório no campo de busca.</strong></p>
            </Fragment>

        )
    }
}

Sidebar.propTypes = {
    position: PropTypes.exact(
        {lng: PropTypes.string, lat: PropTypes.string}
    )
}
