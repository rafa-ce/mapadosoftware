import React from "react";
import { Field, FieldArray } from "formik";
import PropTypes from "prop-types";

const servicosOferecidos = [
    { value: "Instalação e manutenção" },
    { value: "Software as service"},
    { value: "Capacitação/Formação"},
    //{ value: "Licenciamento Se escolher Conteúdo, as opções ser:"},
    { value: "Produção de recursos"},
    { value: "Curadoria de conteúdos"},
    { value: "Formação para uso de conteúdos"},
    { value: "Licenciamento"}
  ];

export const Expertise = (props) => {
    const { values, errors, touched, checkClassName } = props;

    return (
        
        <FieldArray
            name="expertise"
            render={(arrayHelpers) => (
            <div>
                {values.map((software, index) => {
                const errorAreaDeAtuacao =
                    errors?.[index]?.areaDeAtuacao;
                const errorServicoOferecido =
                    errors?.[index]?.servicoOferecido;
                const touchedAreaDeAtuacao =
                    touched?.[index]?.areaDeAtuacao;
                const touchedServicoOferecido =
                    touched?.[index]?.servicoOferecido;
                return (
                    <div key={index} className="pure-control-group">
                        <div className="areaDeAtuacao-wrapper">
                            <div className="areaDeAtuacao-input-wrapper">
                                <Field
                                    as="select"
                                    name={`expertise.${index}.areaDeAtuacao`}
                                    className={
                                        checkClassName(errorAreaDeAtuacao, touchedAreaDeAtuacao)
                                    }
                                >
                                    <option value="">Área de atuação</option>
                                    <option value="Biblioteca/Gestão de recursos educacionais">Biblioteca/Gestão de recursos educacionais</option>
                                    <option value="Ferramentas de comunicação">Ferramentas de comunicação</option>
                                    <option value="E-learning/LMS">E-learning/LMS</option>
                                    <option value="Plataformas de capacitação/Formação">Plataformas de capacitação/Formação</option>
                                    <option value="Hardware e redes">Hardware e redes</option>
                                    <option value="Tecnologia assistiva">Tecnologia assistiva</option>
                                    <option value="Currículo/gestão curricular">Currículo/gestão curricular</option>
                                    <option value="Conteúdo">Conteúdo</option>
                                </Field>
                                {values.length > 1 && (
                                <button
                                    type="button"
                                    title="remover produto"
                                    className="button-remove pure-button"
                                    onClick={() => arrayHelpers.remove(index)}
                                >
                                {" "}
                                -{" "}
                                </button>
                            )}
                        </div>
                        {errorAreaDeAtuacao && touchedAreaDeAtuacao ? (
                        <div className="pure-form-message error">
                            {errorAreaDeAtuacao}
                        </div>
                        ) : null}
                        </div>
                        {
                            values[index]?.areaDeAtuacao !== "" &&
                            <div className="pure-control-group">
                                <label htmlFor={`expertise.${index}.servicoOferecido`}><strong>Para essa área você oferece</strong></label>
                                <FieldArray
                                    name={`expertise.${index}.servicoOferecido`}
                                    render={arrayHelpersServicoOferecido => (
                                    <div>
                                        {servicosOferecidos.map(servico => (
                                        <div key={servico.value}>
                                            <label>
                                                <Field
                                                    className="filled-in"
                                                    name={`expertise.${index}.servicoOferecido.${0}.servicoOferecidoTipo`}
                                                    type="checkbox"
                                                    value={servico.value}
                                                    checked={ 
                                                        values[index]?.servicoOferecido?.filter(s => s.servicoOferecidoTipo === servico.value).length > 0
                                                    }
                                                    onChange={e => {
                                                        if (e.target.checked) {
                                                            arrayHelpersServicoOferecido.push( { servicoOferecidoTipo: servico.value, nomeDoSoftware: "" } );
                                                        }
                                                        else {
                                                            const idx = values[index].servicoOferecido.findIndex((item) => item.servicoOferecidoTipo === servico.value);
                                                            arrayHelpersServicoOferecido.remove(idx);
                                                        }
                                                    }}
                                                    
                                                />
                                                <span>{servico.value}</span>
                                                { 
                                                    (values[index].servicoOferecido.findIndex((item) => item.servicoOferecidoTipo === servico.value) !== -1)  
                                                    &&
                                                    <div className="pure-control-group">
                                                        <Field
                                                            name={`expertise.${index}.servicoOferecido.${values[index].servicoOferecido?.map(function(e) { 
                                                                return e.servicoOferecidoTipo; }).indexOf(servico.value)}.nomeDoSoftware`}
                                                            placeholder="Opcional: Nome/descritivo de serviços/software separado por vírgulas"
                                                            className="pure-input-1 "
                                                        /> 
                                                    </div>
                                                }
                                            </label>
                                        </div>
                                        ))}
                                        {errorServicoOferecido && touchedServicoOferecido ? (
                                        <div className="pure-form-message error">
                                            {errorServicoOferecido}
                                        </div>
                                        ) : null}
                                    </div>
                                    
                                    )}
                                />
                            </div>
                        }
                    </div>
                    );
                })}
                
                <div className="pure-control-group">
                <button
                    type="button"
                    title="adicionar outro area"
                    className="button-add pure-button"
                    onClick={() =>
                    arrayHelpers.push({
                        areaDeAtuacao: "",
                        servicoOferecido: [ ]
                    })
                    }
                >
                    +
                </button>
                <span className="pure-form-message-inline">
                    Adicionar outra Área de Atuação
                </span>
                </div>
            </div>
            )}
        />
    )
}

Expertise.propTypes = {
    values: PropTypes.array,
    errors: PropTypes.array,
    touched: PropTypes.array,
    checkClassName: PropTypes.func
  };