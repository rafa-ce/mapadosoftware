import React, { useState } from 'react';
import './App.css';
import Sidebar from './components/Sidebar';
import Map from './components/Map';

function App() {
  const [position, setPosition] = useState({ lng: null, lat: null })

  return (
    <div className="App">
      <Sidebar position={position} />
      <Map getPosition={setPosition} />
    </div>
  );
}

export default App;
