Configuração de Sistema
======
1. **Faça Um Clone** 
[Faça um clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) do repositório (geralmente será um [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) do projeto original)

2. **O projeto tem dois aplicativos** 
Um servidor de API criado com [Express](https://expressjs.com/) e uma interface para o usuário criada com [React](https://reactjs.org/).

3. **Requirimentos**
    * [NODE LTS](https://nodejs.org/en/)
    * [NPM](https://www.npmjs.com/) ou [YARN](https://yarnpkg.com/)
    * MongoDB - Nós usamos uma conta gratuita no [Mongodb.com](https://www.mongodb.com/cloud/atlas/register) mas você pode [instalar localmente também](https://docs.mongodb.com/manual/installation/)

4. **Configure o DB** use como base o `.env.exemplo` (encontrado na pasta `api`) para criar o seu próprio `.env` indicando o DB para desenvolvimento.

5. **Faça instalação dos Node modules**
Cada um dos aplicativos requer instalação. e.g. `yarn --cwd client install` & `yarn --cwd api install` 

6. **Inicie os dois aplicativos** 
Abra uma aba no seu terminal e inicie o client: `yarn --cwd client start`. Abra outra aba para a API: `yarn --cwd api start`.

7. **Endpoints**
O seu aplicativo cliente estará rodando em `localhost:3000`. A API pode ser acessado usando `localhost:9000`. Usamos só dois *endpoints* no momento: `POST` para criar um - `localhost:9000/api/point` e `GET` todos os pontos `localhost:9000/api/points`.
